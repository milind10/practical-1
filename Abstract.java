abstract class vehicles{
	public void show(){
		System.out.print("This is abstract class and can't be instantiated");
	}
	abstract int noOfWheels();
}
class Truck extends vehicles{
	public void show(){
		System.out.println("This is Truck class");
	}
	public int noOfWheels(){
		return 8;
	}
}
class Car extends vehicles{
	public void show(){
		System.out.println("This is car class");
	}
	public int noOfWheels(){
		return 4;
	}
}
public class Abstract{
	public static void main(String[] args){
		Truck t1 = new Truck();
		System.out.println(t1.noOfWheels());
    Car c1 = new Car();
    System.out.println(c1.noOfWheels());
	}
}//This file contains example of abstraction in java


