class Person{
	showmsg(){
		console.log("This is Person class"); 
	}
}
class Employee extends Person{
	showmsg(){
		console.log("This is employee class");
	}
}
const e1 = new Employee();
e1.showmsg();
