class Parent{
	public void showmsg(){
		System.out.println("This is method of parent class");
	}
}
class Child extends Parent{
	public void showmsg(){
		System.out.println("This is child class");
	}
}
public class Inheritance{
  public static void main(String[] args){
    Child child1 = new Child();
    child1.showmsg();
  }
}
//this also contains the example of dynamic polymorphism
