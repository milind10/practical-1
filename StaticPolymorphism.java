class Multiply{
  public int multiply(int a,int b,int c){
    return a*b*c;
  }
  public int multiply(int a,int b){
    return a*b;
  }
}
//method overloading compile time polymorphism
public class StaticPolymorphism{
  public static void main(String[] args){
    Multiply object1 = new Multiply();
    System.out.println(object1.multiply(1,2,4));
    Multiply object2 = new Multiply();
    System.out.println(object1.multiply(1,2));
  }
}
