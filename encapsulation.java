class Student {

	private String studentName;
	private int studentRollno;
	private int studentAge;

	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
  }
	public int getStudentRollno() {
		return studentRollno;
	}
	public void setStudentRollno(int studentRollno) {
		this.studentRollno = studentRollno;
	}
	public int getStudentAge() {
		return studentAge;
	}
	public void setStudentAge(int studentAge) {
		this.studentAge = studentAge;
	}
	
}
public class encapsulation{

	public static void main(String args[]) {
		Student st1 = new Student();

		st1.setStudentName("MDJ");
		st1.setStudentRollno(101);
		st1.setStudentAge(22);

		// Printing the values of the variables

		System.out.println("Student Name : " + st1.getStudentName());
		System.out.println("Student Rollno : " + st1.getStudentRollno());
		System.out.println("Student Age : " + st1.getStudentAge());
	}

}
