class Person{
	constructor(name,age){
		this.real_name = name;
		this.age=age;
	}
}
class Employee extends Person{
	constructor(name,age,id,domain){
		super(name,age);
		this.id=id;
		this.domain=domain;
	}
	get_domain(){
		return this.domain; 	
	}
}
const e1 = new Employee("milind","22","65","developer");
console.log(e1.get_domain());
